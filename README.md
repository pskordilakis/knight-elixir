# Knight

Program written in elixir for the purpose of examining the languages basic
features(no concurency)

## Problem
Determine if a knight chess piece can reach a point in the board from a specific
point within a number of moves

## Solution
The problem is a classic pathfinding problem, but the approach is more simpler.
The project consists of a single module, Knight. The module defines a public
function(move) which can be called like

```Knight.move from: {x, y}, to: {x1, y1}, within n```

If successful ``{:ok, number_of_moves}`` will be returned, otherwise
``{:error, "No route"}``.

The parameters of the move function are passed using a keyword list, the
simplest way of making a DSL(Domain Specific Language) in elixir.

The move function depends(calles) on one private function(next_moves) which
computes all the possible positions the knight can occupy in the range 1..n
moves. The function accepts a list of positions and returns a
map(n => [positions]). In that way it can be called recursively to solve the
problem. Finally the next_moves function depends on another private function
knight moves which accepts a single position and returns a list with the
possible positions the knight can move to.

## Tests
Project has two tests one successful and one that fails. To run the tests run
``mix tests``
