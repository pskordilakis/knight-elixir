defmodule KnightTest do
  use ExUnit.Case
  doctest Knight

  test "Knight in {4, 4} reaches {6, 5} in one move" do
    Knight.move from: {4, 4}, to: {6, 5}, within: 5 == {:ok, 1}
  end

  test "Knight in {4, 4} can not reach {6, 8} in two moves" do
    Knight.move from: {4, 4}, to: {6, 5}, within: 5 == {:error, "No route"}
  end
end
