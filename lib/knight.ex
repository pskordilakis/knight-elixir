defmodule Knight do
  @moduledoc """
  Provides a function related to the knight chess piece

  ## Examples

      iex> Knight.move from: {4, 4}, to: {6, 5}, within: 5
      {:ok, 1}

  """

  @doc """
  Calculates if a knight piece(chess) can go from one position to another within
  a curtain amount of hops.

  Returns
      {:ok, number_of_moves}
  on success. Otherwise
      {:error, "No route"}
  """
  def move([from: starting_position, to: finish_position, within: moves]) do
    try do
      res = next_moves([starting_position], moves, Map.new)
        |> Enum.filter(fn {_,v} -> Enum.member?(v, finish_position) end)
        |> Enum.min_by(fn {k, _} -> k end)

      {:ok, elem(res, 0) + 1}
    rescue
      _ -> {:error, "No route"}
     end
  end

  # no more moves left return acc
  defp next_moves(_, 0, acc) do
    acc
  end

  #compute next positions for each position in the list list
  defp next_moves(positions, moves, acc) do
    res =
      positions
        |> Enum.map(&knight_moves/1)
    next_moves(hd(res), moves-1, Map.put(acc, Map.size(acc), hd(res)))
  end

  #compute next possible positions[y1..yn] from position x
  defp knight_moves(position) do
    [{2, 1}, {2, -1}, {1, 2}, {-1, 2}, {-2, 1}, {-2, -1}, {1, -2}, {-1, -2}]
      |> Enum.map(&({elem(position, 0) + elem(&1, 0), elem(position, 1) + elem(&1, 1)}))
      |> Enum.filter(&(elem(&1,0) > 0 and elem(&1, 0) < 9 and elem(&1, 1) > 0 and elem(&1,1) < 9 ))
  end
end
